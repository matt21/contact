<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Document\Contact;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/contact/")
 */
class AdminContactController extends Controller
{
    /**
     * Controller action responsible for listing paginated contacts
     * @param Request $request
     * @return Response
     *
     *
     * @Route("list", name="admin_contact_list")
     */
    public function listAction(Request $request): Response
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $contactsQuery = $dm->getRepository(Contact::class)
            ->createQueryToFindAll();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $contactsQuery,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 10)
        );

        return $this->render('admin/contact/list.html.twig',[
            'pagination' => $pagination,
        ]);
    }
}
