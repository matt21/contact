<?php

namespace AppBundle\Controller;

use AppBundle\Form\ContactForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends Controller
{
    /**
     * Controller action responsible for rendering and handling form
     *
     * @Route("/contact", name="user_contact")
     */
    public function contactAction(Request $request): Response
    {
        $form = $this->createForm(ContactForm::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $dm = $this->get('doctrine_mongodb')->getManager();
            $contact = $form->getData();

            //check honeycaptcha
            if($form->get('email_confirmation')->getData() !== null){
                $this->addFlash(
                    'error',
                    'Your message is spam'
                );
                return $this->redirectToRoute('user_contact');
            }

            $dm->persist($contact);
            $dm->flush();

            $this->addFlash(
                'success',
                'Your message has been submitted successfully'
            );

            return $this->redirectToRoute('user_contact');
        }

        return $this->render('contact/contact.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
