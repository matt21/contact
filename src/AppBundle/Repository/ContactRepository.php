<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 16.11.17
 * Time: 00:24
 */

namespace AppBundle\Repository;


use Doctrine\ODM\MongoDB\DocumentRepository;

class ContactRepository extends DocumentRepository
{
    public function createQueryToFindAll()
    {
        return $this->createQueryBuilder()
            ->getQuery();
    }
}