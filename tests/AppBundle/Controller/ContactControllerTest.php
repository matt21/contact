<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ContactControllerTest extends WebTestCase
{
    /**
     * Method to test if contact page contains contact form
     */
    public function testContactPage()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/contact');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Contact form', $crawler->filter('h3')->first()->text());
        $this->assertEquals('app_bundle_contact_form', $crawler->filter('form')->first()->attr('name'));
        $this->assertEquals('app_bundle_contact_form_name', $crawler->filter('input')->eq(0)->attr('id'));
        $this->assertEquals('app_bundle_contact_form_email', $crawler->filter('input')->eq(1)->attr('id'));
        $this->assertEquals('app_bundle_contact_form_email_confirmation', $crawler->filter('input')->eq(2)->attr('id'));
        $this->assertEquals('hidden', $crawler->filter('input')->eq(2)->attr('type'));
        $this->assertEquals('app_bundle_contact_form_message', $crawler->filter('input')->eq(3)->attr('id'));
    }

    /**
     * Method to test form uri and HTTP method
     */
    public function testForm()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/contact');

        $form = $crawler->selectButton('Send')->form();

        $uri = $form->getUri();
        $this->assertRegExp('/contact/', $uri);

        $method = $form->getMethod();
        $this->assertEquals('POST', $method);
    }

    /**
     * Method to test successful form submission
     */
    public function testFormSubmissionSuccess()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/contact');

        $form = $crawler->selectButton('Send')->form();
        $form->setValues([
            'app_bundle_contact_form[name]' => "Andrew",
            'app_bundle_contact_form[email]' => "andrew@gmail.com",
            'app_bundle_contact_form[message]' => "Lorem ipsum dolor es sumit",
        ]);

        $client->submit($form);
        $client->followRedirect();
        $response = $client->getResponse();
        $this->assertContains('Your message has been submitted successfully', $response->getContent());
    }

    /**
     * Method to test honeycaptcha
     */
    public function testFormSubmissionSpam()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/contact');

        $form = $crawler->selectButton('Send')->form();
        $form->setValues([
            'app_bundle_contact_form[name]' => "Andrew",
            'app_bundle_contact_form[email]' => "andrew@gmail.com",
            'app_bundle_contact_form[email_confirmation]' => "andrew@gmail.com",
            'app_bundle_contact_form[message]' => "Lorem ipsum dolor es sumit",
        ]);

        $client->submit($form);
        $client->followRedirect();
        $response = $client->getResponse();
        $this->assertContains('Your message is spam', $response->getContent());
    }

    /**
     * Method to test form errors
     */
    public function testFormSubmissionError()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/contact');

        $form = $crawler->selectButton('Send')->form();
        $form->setValues([
            'app_bundle_contact_form[name]' => "",
            'app_bundle_contact_form[email]' => "aaa",
            'app_bundle_contact_form[message]' => "aa",
        ]);

        $client->submit($form);
        $response = $client->getResponse();
        $this->assertContains('This field can not be empty', $response->getContent());
        $this->assertContains('Provide valid email', $response->getContent());
        $this->assertContains('Minimum number of characters for this field is 3', $response->getContent());
    }
}
