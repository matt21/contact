<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 15.11.17
 * Time: 22:23
 */

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @MongoDB\Document(collection="contacts", repositoryClass="AppBundle\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @Assert\NotBlank(message="This field can not be empty")
     * @Assert\Length(
     *     min="2",
     *     minMessage="Minimum number of characters for this field is 2",
     *     max="30",
     *     maxMessage="Maximum number of characters for this field is 30"
     * )
     */
    protected $name;

    /**
     * @MongoDB\Field(type="string")
     * @Assert\NotBlank(message="This field can not be empty")
     * @Assert\Email(message="Provide valid email")
     */
    protected $email;

    /**
     * @MongoDB\Field(type="string")
     * @Assert\NotBlank(message="This field can not be empty")
     * @Assert\Length(
     *     min="3",
     *     minMessage="Minimum number of characters for this field is 3",
     *     max="300",
     *     maxMessage="Maximum number of characters for this field is 300"
     * )
     */
    protected $message;

    /**
     * @MongoDB\Field(type="date")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

}