Symfony Standard Edition
========================

Welcome to the Symfony Standard Edition - a fully-functional Symfony
application that you can use as the skeleton for your new applications.

For details on how to download and get started with Symfony, see the
[Installation][1] chapter of the Symfony Documentation.

What's inside?
--------------

The Symfony Standard Edition is configured with the following defaults:

  * An AppBundle you can use to start coding;

  * Twig as the only configured template engine;

  * Doctrine ORM/DBAL;

  * Swiftmailer;

  * Annotations enabled for everything.

It comes pre-configured with the following bundles:

  * **FrameworkBundle** - The core Symfony framework bundle

  * [**SensioFrameworkExtraBundle**][6] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**][7] - Adds support for the Doctrine ORM

  * [**TwigBundle**][8] - Adds support for the Twig templating engine

  * [**SecurityBundle**][9] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][10] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][11] - Adds support for Monolog, a logging library

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**][1] (in dev env) - Adds code generation
    capabilities

  * [**DoctrineMongoDBBundle**][15] - Adds support for mongoDB

  * [**StofDoctrineExtensionsBundle**][16] - Extends doctrine functionality
    
  * [**FpJsFormValidatorBundle**][17] - Validates forms thru javascript
    
  * [**AsseticBundle**][18] - Manages assets
    
  * [**KnpPaginatorBundle**][19] - Pagination finctionality

All libraries and bundles included in the Symfony Standard Edition are
released under the MIT or BSD license.

Enjoy!


**Installation**:

**MongoDB installation:**

In order for MongoDB to work You have to have it installed:

import public key
  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10

Create a list file for MongoDB.
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list

Reload local package database.
sudo apt-get update

Install old packages
sudo apt-get install mongodb

Install the MongoDB newest packages.
sudo apt-get install -y mongodb-org
 
You also have to have installed php mongo extension:

sudo apt-get install php7.0-mongodb   or   sudo apt-get install php-mongodb

**Project Installation**

In order to install project run:
composer install

and setup all parameters

**Setup database and load fixtures**

php bin/console doctrine:mongodb:schema:create 
php bin/console doctrine:mongodb:fixtures:load

**Setup server local hosts or run php server**

php bin/console server:run
Now check out the site at `http://localhost:8000`

Have fun!

[1]:  https://symfony.com/doc/3.3/setup.html
[6]:  https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/index.html
[7]:  https://symfony.com/doc/3.3/doctrine.html
[8]:  https://symfony.com/doc/3.3/templating.html
[9]:  https://symfony.com/doc/3.3/security.html
[10]: https://symfony.com/doc/3.3/email.html
[11]: https://symfony.com/doc/3.3/logging.html
[13]: https://symfony.com/doc/current/bundles/SensioGeneratorBundle/index.html
[14]: https://symfony.com/doc/current/setup/built_in_web_server.html
[15]: http://symfony.com/doc/current/bundles/DoctrineMongoDBBundle/index.html
[16]: https://symfony.com/doc/master/bundles/StofDoctrineExtensionsBundle/index.html
[17]: https://github.com/formapro/JsFormValidatorBundle
[18]: https://github.com/symfony/assetic-bundle
[19]: https://github.com/KnpLabs/KnpPaginatorBundle
